﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MSPAPI.Models;
//using MSPAPI.SignalR;
using MSPAPI.ViewModel;

using System.Net.Mail;
using System.Web;
using System.Data.Entity;
using System.Xml;
using System.Threading;
using System.IO;
using System.Configuration;

namespace MSPAPI.Controllers
{
    public class GuardianController : ApiController//WithHub<PickMeUpHub>
    {
        bool isDevMode = Convert.ToBoolean(ConfigurationManager.AppSettings["DevMode"]);
        MspTestPortalEntities _context = new MspTestPortalEntities();

        public GuardianController()
        {
            if (isDevMode)
            {
                _context.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }

        [HttpPost]
        public void UpdateDeviceId(GuardianViewModel model)
        {
            try
            {
                var objGuardian = _context.Guardians
                     .FirstOrDefault(x => x.Id == model.Id);
                if (objGuardian != null)
                {
                    objGuardian.DeviceId = model.DeviceId;
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public IHttpActionResult DeviceRegistration(GuardianViewModel model)
        {
            try
            {
                var objGuardian = _context.Guardians
                     .FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber);
                if (objGuardian != null)
                {
                    if (model.DeviceId != null)
                    {
                        objGuardian.DeviceId = model.DeviceId;
                        _context.SaveChanges();
                    }
                    string status = SendOtp(model.PhoneNumber);
                    //if(status== "ExceptionOccuredInSmsGateWay")
                    //{

                    //}
                    //if (status != "0")
                    //{
                    //    return Json("OtpSendingFailed");
                    //}
                    return Json(objGuardian.Id);
                }
                else
                {
                    return Json("PhNoNotExists");
                }

            }
            catch (Exception ex)
            {

                return Json("");
            }
        }
        [HttpPost]
        public double DistanceCalculate(LatLangViewModel model)
        {
            double SchoolLattitude = 0;
            double SchoolLongitude = 0;
            if (isDevMode)
            {
                //Nampally Station Co-Ordinates
                //SchoolLattitude = 17.3946;
                //SchoolLongitude = 78.4676;


                //Cat Technologies Co-Ordinates
                SchoolLattitude = 17.3887;
                SchoolLongitude = 78.4737;
                    
            }
            else
            {
                //SchoolLattitude = 26.289421;
                //SchoolLongitude = 50.214788;

                SchoolLattitude = 25.217456;
                SchoolLongitude = 55.282746;
            }
            try
            {
                double distance = Calculate(SchoolLattitude, SchoolLongitude, model.CurrentLattitude, model.CurrentLongitude);
                if (distance < 1500)
                {
                    SendNearByNotification(model.GuardianId, distance);
                }

                return distance;
            }
            catch (Exception ex)
            {
                return 0;
                throw ex;
            }

        }
        private void SendNearByNotification(Guid guardianId, double distance)
        {
            var todayDate = DateTime.Now.Date;
            var nearByNotification = _context.Notifications.FirstOrDefault(x => x.GuardianId == guardianId && DbFunctions.TruncateTime(x.NotificationDate) == DbFunctions.TruncateTime(todayDate) && (x.InStatus == "NearBy"));
            var inPremisesNotification = _context.Notifications.FirstOrDefault(x => x.GuardianId == guardianId && DbFunctions.TruncateTime(x.NotificationDate) == DbFunctions.TruncateTime(todayDate) && (x.InStatus == "InPremises"));
            var guardian = _context.Guardians.FirstOrDefault(x => x.Id == guardianId);
            //var studentList = _context.Students.Where(x => x.GuardianId == guardianId).ToList();
            
            var studentList = (from sg in _context.StudentGuardians
                               join s in _context.Students on sg.StudentId equals s.Id
                               where sg.GuardianId == guardianId
                               select s).ToList();


            if (nearByNotification == null || inPremisesNotification == null)
            {
                if (distance < 100 && inPremisesNotification == null)
                {
                    foreach (var student in studentList)
                    {
                        var sectionId = Convert.ToInt32(student.SectionId);
                        var section = _context.Sections.FirstOrDefault(x => x.SectionId == sectionId);
                        string guardianType = "";
                        if (guardian.GuardianType == "Father")
                            guardianType = "<b style='color:#34aa89'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";
                        else if (guardian.GuardianType == "Mother")
                            guardianType = "<b style='color:#EA4335'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";
                        else
                            guardianType = "<b style='color:#4285f4'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";

                        string message = guardianType + " of " + student.FirstName.Trim() + " " + student.LastName.Trim() + " of Grade " + section.ClassId + section.SectionName + " has reached school premises at " + DateTime.Now;
                        Notification notification = SaveNotification(guardian.Id, message, student.Grade, student.SectionId, student.Id, "InPremises");

                    }

                }
                else if (distance < 1500 && distance > 100 && nearByNotification == null)
                {
                    foreach (var student in studentList)
                    {
                        var sectionId = Convert.ToInt32(student.SectionId);
                        var section = _context.Sections.FirstOrDefault(x => x.SectionId == sectionId);
                        string guardianType = "";
                        if (guardian.GuardianType == "Father")
                            guardianType = "<b style='color:#34aa89'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";
                        else if (guardian.GuardianType == "Mother")
                            guardianType = "<b style='color:#EA4335'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";
                        else
                            guardianType = "<b style='color:#4285f4'>" + guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName + "</b>";

                        string message = guardianType + " of " + student.FirstName.Trim() + " " + student.LastName.Trim() + " of Grade " + section.ClassId + section.SectionName +  " coming near to school at " + DateTime.Now;
                        Notification notification = SaveNotification(guardian.Id, message, student.Grade, student.SectionId, student.Id, "NearBy");

                    }

                }
            }

        }

        public string getTime(TimeSpan? outTime)
        {
            int Hours = 0;
            int Min = 0;


            if (outTime.HasValue)
            {
                Hours = outTime.Value.Hours;
                Min = outTime.Value.Minutes;
                if (Hours > 11)
                {
                    if (Hours - 12 > 9)
                        if (Min > 9)
                            return (Hours - 12).ToString() + ":" + Min.ToString() + " PM";
                        else
                            return (Hours - 12).ToString() + ":0" + Min.ToString() + " PM";
                    else
                         if (Min > 9)
                        return "0" + (Hours - 12).ToString() + ":" + Min.ToString() + " PM";
                    else
                        return "0" + (Hours - 12).ToString() + ":0" + Min.ToString() + " PM";
                }
                else
                {
                    if (Hours <= 9)
                        if (Min > 9)
                            return "0" + Hours.ToString() + ":" + Min.ToString() + "AM";
                        else
                            return "0" + Hours.ToString() + ":0" + Min.ToString() + "AM";
                    else
                         if (Min > 9)
                        return Hours.ToString() + ":" + Min.ToString() + "AM";
                    else
                        return Hours.ToString() + ":0" + Min.ToString() + "AM";

                }
            }
            else
            {
                return "";
            }

            return "";
        }
        [HttpPut]
        public IHttpActionResult GetStudentsByGuardianId1(GuardianViewModel data)
        {
            var studentList = new List<StudentViewModel>();

            var QueueStudentList = new List<StudentViewModel>();
            var DissmissStudentList = new List<StudentViewModel>();
            var PickedStudentList = new List<StudentViewModel>();


            try
            {
                var todaydate = DateTime.Now.Date.AddDays(-5);



                studentList = (from s in _context.Students
                                   //join g in _context.Guardians on s.GuardianId equals g.Id
                               join ss in _context.StudentStatus on s.Id equals ss.StudentId
                               join sg in _context.StudentGuardians on s.Id equals sg.StudentId
                               join g in _context.Guardians on sg.GuardianId equals g.Id
                               where ss.CurrentDate >= todaydate && sg.GuardianId == data.Id
                               select new StudentViewModel
                               {
                                   Id = s.Id,
                                   FirstName = s.FirstName,
                                   LastName = s.LastName,
                                   MiddleName = s.MiddleName,
                                   FullName = s.FirstName + " " + s.LastName,
                                   Grade = s.Grade + "Grade",
                                   Section = "section" + s.SectionId,
                                   Status = (int)ss.Status,
                                   //WaitingTime = ss.WaitingTime.HasValue ? ss.WaitingTime.Value.Hours.ToString() + ":" + ss.WaitingTime.Value.Minutes.ToString() : "",
                                   //QueueTime = ss.QueueTime.HasValue ? ss.QueueTime.Value.Hours.ToString() + ":" + ss.QueueTime.Value.Minutes.ToString() : "",
                                   //PickedUpTime = ss.PickedUpTime.HasValue ? ss.PickedUpTime.Value.Hours.ToString() + ":" + ss.PickedUpTime.Value.Minutes.ToString() : "",
                                   //DismissTime = ss.DismissTime.HasValue ? ss.DismissTime.Value.Hours.ToString() + ":" + ss.DismissTime.Value.Minutes.ToString() : "",
                                   WaitingTimeSpan = ss.WaitingTime,
                                   QueueTimeSpan = ss.QueueTime,
                                   PickedUpTimeSpan = ss.PickedUpTime,
                                   DismissTimeSpan = ss.DismissTime,
                                   CurrentDate = ss.CurrentDate,
                                   StatusDate = ss.CurrentDate.ToString(),
                                   GateName = ss.GateId.HasValue ? "Gate" + ss.GateId.ToString() : ""

                               }).ToList();

                foreach (StudentViewModel item in studentList)
                {
                    item.StatusDate = item.CurrentDate.ToString("dd/MM/yyyy");

                    item.WaitingTime = getTime(item.WaitingTimeSpan);
                    item.QueueTime = getTime(item.QueueTimeSpan);
                    item.PickedUpTime = getTime(item.PickedUpTimeSpan);
                    item.DismissTime = getTime(item.DismissTimeSpan);

                }


                List<DateTime> dateList = new List<DateTime>();
                for (int i = 0; i < 5; i++)
                {
                    dateList.Add(DateTime.Now.Date.AddDays(-i));
                }

                var queuedStudList = dateList.Select(x =>
                new StudentsByDateViewModel
                {
                    Date = x.Date.ToShortDateString(),
                    StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
                    y.Status == (int)StudentStatus.InQueue).OrderByDescending(y => y.QueueTimeSpan).ToList()
                }).Where(x => x.StudentList.Count > 0).ToList();

                var dismissStudList = dateList.Select(x =>
                new StudentsByDateViewModel
                {
                    Date = x.Date.ToShortDateString(),
                    StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
                    y.Status == (int)StudentStatus.Dismiss).OrderByDescending(y => y.DismissTimeSpan).ToList()
                }).Where(x => x.StudentList.Count > 0).ToList();

                var pickedStudList = dateList.Select(x =>
                new StudentsByDateViewModel
                {
                    Date = x.Date.ToShortDateString(),
                    StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
                    y.Status == (int)StudentStatus.PickedUp).OrderByDescending(y => y.PickedUpTimeSpan).ToList()
                }).Where(x => x.StudentList.Count > 0).ToList();

                Tuple<List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, string, string> tuple =
                                 new Tuple<List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, List<StudentsByDateViewModel>, string, string>
                                 (queuedStudList, dismissStudList, pickedStudList, DateTime.Now.ToShortDateString(), DateTime.Now.AddDays(-1).ToShortDateString());



                return Json(tuple);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpPut]
        public IHttpActionResult GetStudentsByGuardianId(GuardianViewModel data)
        {
            var studentList = new List<StudentViewModel>();

            var QueueStudentList = new List<StudentViewModel>();
            var DissmissStudentList = new List<StudentViewModel>();
            var PickedStudentList = new List<StudentViewModel>();


            try
            {
                var todaydate = DateTime.Now.Date.AddDays(-5);



                studentList = (from s in _context.Students
                                   //join g in _context.Guardians on s.GuardianId equals g.Id
                               join ss in _context.StudentStatus on s.Id equals ss.StudentId
                               join sg in _context.StudentGuardians on s.Id equals sg.StudentId
                               join g in _context.Guardians on sg.GuardianId equals g.Id
                               where ss.CurrentDate >= todaydate && sg.GuardianId == data.Id
                               select new StudentViewModel
                               {
                                   Id = s.Id,
                                   FirstName = s.FirstName,
                                   LastName = s.LastName,
                                   MiddleName = s.MiddleName,
                                   FullName = s.FirstName + " " + s.LastName,
                                   Grade = s.Grade + "Grade",
                                   Section = "section" + s.SectionId,
                                   Status = (int)ss.Status,
                                   //WaitingTime = ss.WaitingTime.HasValue ? ss.WaitingTime.Value.Hours.ToString() + ":" + ss.WaitingTime.Value.Minutes.ToString() : "",
                                   //QueueTime = ss.QueueTime.HasValue ? ss.QueueTime.Value.Hours.ToString() + ":" + ss.QueueTime.Value.Minutes.ToString() : "",
                                   //PickedUpTime = ss.PickedUpTime.HasValue ? ss.PickedUpTime.Value.Hours.ToString() + ":" + ss.PickedUpTime.Value.Minutes.ToString() : "",
                                   //DismissTime = ss.DismissTime.HasValue ? ss.DismissTime.Value.Hours.ToString() + ":" + ss.DismissTime.Value.Minutes.ToString() : "",
                                   WaitingTimeSpan = ss.WaitingTime,
                                   QueueTimeSpan = ss.QueueTime,
                                   PickedUpTimeSpan = ss.PickedUpTime,
                                   DismissTimeSpan = ss.DismissTime,
                                   CurrentDate = ss.CurrentDate,
                                   StatusDate = ss.CurrentDate.ToString(),
                                   GateName = ss.GateId.HasValue ? "Gate" + ss.GateId.ToString() : "",
                                   UpdatedTime=ss.UpdatedTime

                               }).ToList();

                foreach (StudentViewModel item in studentList)
                {
                    item.StatusDate = item.CurrentDate.ToString("dd/MM/yyyy");

                    item.WaitingTime = getTime(item.WaitingTimeSpan);
                    item.QueueTime = getTime(item.QueueTimeSpan);
                    item.PickedUpTime = getTime(item.PickedUpTimeSpan);
                    item.DismissTime = getTime(item.DismissTimeSpan);

                }


                List<DateTime> dateList = new List<DateTime>();
                for (int i = 0; i < 5; i++)
                {
                    dateList.Add(DateTime.Now.Date.AddDays(-i));
                }

                var queuedStudList = dateList.Select(x =>
                new StudentsByDateViewModel
                {
                    Date = x.Date.ToShortDateString(),
                    StudentList = studentList.Where(y => y.CurrentDate == x.Date &&
                    y.Status != (int)StudentStatus.Waiting).OrderByDescending(y => y.UpdatedTime).ToList()
                }).Where(x => x.StudentList.Count > 0).ToList();

                

                Tuple<List<StudentsByDateViewModel>,  string, string> tuple =
                                 new Tuple<List<StudentsByDateViewModel>,  string, string>
                                 (queuedStudList,  DateTime.Now.ToShortDateString(), DateTime.Now.AddDays(-1).ToShortDateString());



                return Json(tuple);
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string getSectionName(int GradeId, int SectionId)
        {
            var sectionname = _context.Sections.Where(x => x.ClassId == GradeId && x.SectionId == SectionId).FirstOrDefault();
            return sectionname.SectionName;
        }
        public string getSectionName1(string SectionId)
        {
            int sectionId = Convert.ToInt32(SectionId);
            var sectionname = _context.Sections.Where(x => x.SectionId == sectionId).FirstOrDefault();
            return sectionname.SectionName;
        }


        // POST api/<controller>
        [System.Web.Mvc.HttpPost]
        public IHttpActionResult UpdateStudentPickedStatus(StudentViewModel obj)
        {
            try
            {

                var todaydate = DateTime.Now.Date;
                var studentStatus = _context.StudentStatus.FirstOrDefault(x => x.StudentId == obj.Id && x.CurrentDate == todaydate);
                if (studentStatus != null)
                {
                    //studentStatus.Status = 4;
                    studentStatus.Status = (int)StudentStatus.PickedUp;
                    studentStatus.PickedUpTime = DateTime.Now.TimeOfDay;
                    studentStatus.UpdatedTime = DateTime.Now;
                    studentStatus.PickedBy = obj.GuardianId;
                    _context.SaveChanges();
                }
                var pickedStudent = (from s in _context.Students
                                     join ss in _context.StudentStatus on s.Id equals ss.StudentId
                                     where ss.CurrentDate == todaydate && ss.Status == (int)StudentStatus.PickedUp && s.Id == obj.Id
                                     select new StudentViewModel
                                     {
                                         Id = s.Id,
                                         FirstName = s.FirstName,
                                         DOB = s.DOB,
                                         Grade = s.Grade,
                                         LastName = s.LastName,
                                         MiddleName = s.MiddleName,
                                         Status = (int)ss.Status,
                                         GuardianId = obj.GuardianId,
                                         Section = s.SectionId,

                                     }).FirstOrDefault();


                var guardian = _context.Guardians.FirstOrDefault(x => x.Id == pickedStudent.GuardianId);
                pickedStudent.SectionName = getSectionName(Convert.ToInt16(pickedStudent.Grade), Convert.ToInt16(pickedStudent.Section));





                var NewNotification = new Notification();
                NewNotification.StudentId = pickedStudent.Id;
                NewNotification.GuardianId = pickedStudent.GuardianId;
                NewNotification.Message = guardian.GuardianType + " " + guardian.FirtName.Trim() + " " + guardian.LastName.Trim() + " picked up " + pickedStudent.FirstName + " " + pickedStudent.LastName + " of Grade " + pickedStudent.Grade + pickedStudent.SectionName + " at "+ DateTime.Now;
                NewNotification.InStatus = "PickedUp";
                NewNotification.NotificationDate = DateTime.Now;
                NewNotification.SectionId = Convert.ToInt32(pickedStudent.Section);
                NewNotification.GradeId = Convert.ToInt32(pickedStudent.Grade);
                NewNotification.NotificationFrom = "Mobile App";
                NewNotification.GateId = Convert.ToInt32(obj.GateName.Replace("Gate", ""));
                NewNotification.IsNotified = false;
                _context.Notifications.Add(NewNotification);

                _context.SaveChanges();

                Tuple<StudentViewModel, Notification> tuple =
                               new Tuple<StudentViewModel, Notification>
                               (pickedStudent, NewNotification);





                return Json(tuple);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string getTimeInAMPM(string datetime)
        {
            //DateTime dt1 = DateTime.Parse(dt.ToString());
            //Console.Write(dt.ToString("HH:mm"));
            //return dt1.ToString("HH:mm");.
            DateTime dt;
            string str = string.Empty;
            if (DateTime.TryParse(datetime, out dt))
            {
                str = dt.ToString("hh:mm tt");
            }


            return str;

        }
        [HttpPut]
        public IHttpActionResult GetNotificationsByGuardianId(GuardianViewModel model)
        {
            try
            {
                var todaydate = DateTime.Now.Date.AddDays(-7);

                var notificationList = _context.Notifications.Where(x => x.GuardianId == model.Id && x.NotificationDate >=todaydate )
                              .OrderByDescending(x => x.NotificationDate).ToList().Select(x =>
                                new NotificationViewModel
                                {
                                    Id = x.Id,
                                    GateName = "Gate" + x.GateId,
                                    StudentName = x.Student.FirstName + " " + x.Student.LastName,
                                    Grade = x.GradeId + "Grade",
                                    Status = x.InStatus,
                                    Section = "section" + x.SectionId,
                                    //NotificationTime = x.NotificationDate.Value.ToShortTimeString(),
                                    //DateTime dt = DateTime.Parse("6/22/2009 07:00:00 PM");
                                    //Console.Write(dt.ToString("HH:mm"));

                                    NotificationTime = getTimeInAMPM(x.NotificationDate.Value.ToString()),//DateTime.Parse(x.NotificationDate.ToString("HH:mm")).ToString(),
                                    NotificationDate = x.NotificationDate.Value.ToShortDateString(),
                                    Message = x.Message,
                                    FirstName = x.Student.FirstName,
                                    LastName = x.Student.LastName

                                });
                var inQueueNotList = notificationList.Where(x => x.Status == "InQueue").ToList();
                var dismissedNotList = notificationList.Where(x => x.Status == "In Dismiss").ToList();
                var pickedUpNotList = notificationList.Where(x => x.Status == "PickedUp").ToList();
                Tuple<List<NotificationViewModel>, List<NotificationViewModel>, List<NotificationViewModel>> tuple =
                                new Tuple<List<NotificationViewModel>, List<NotificationViewModel>, List<NotificationViewModel>>
                                (inQueueNotList, dismissedNotList, pickedUpNotList);
                return Json(tuple);
            }
            catch (Exception ex)
            {
                return Json("error");
            }

        }

        [HttpPost]
        public IHttpActionResult AddReportProblem(ReportProblemViewModel model)
        {
            try
            {
                var reportProblem = new ReportProblem
                {
                    Name = model.Name,
                    Message = model.Message,
                    Email = model.Email,
                    ReporterId = model.ReporterId

                };
                _context.ReportProblems.Add(reportProblem);
                _context.SaveChanges();

                var body = "<p>Hi Admin</p><p><p>You have received feedback from user " + model.Name + "</p>";
                body = body + "<p>" + model.Message + "</p><p>Best Regards</p><p>Misk School Team</p>";
                var confirmationMsg = "<p>Hi <b>" + model.Name + "</b>,</p><p>Your request has been sent successfully.We will get back to you soon.</p><p>Best Regards</p><p>Misk School Team</p>";
                if (isDevMode)
                {
                    SendFeedBack("Rammohan.test1@gmail.com", body);
                }
                else
                {
                    SendFeedBack("miskschooltesting@gmail.com", body);
                }
                SendFeedBack(model.Email, confirmationMsg);
            }
            catch (Exception ex)
            {
                return Json("error");
            }
            return Json("success");
        }
        public void SendFeedBack(string email, string body)
        {
            Thread thread = new Thread(delegate ()
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(new MailAddress(email));
                //replace with valid value
                mailMessage.Subject = "Feedback";
                mailMessage.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                using (var smtp = new SmtpClient())
                {

                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mailMessage);
                }
            });
            thread.IsBackground = true;
            thread.Start();


        }
        public void SendOtpToEmail(string email, string body)
        {
            Thread thread = new Thread(delegate ()
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(new MailAddress(email));
                //replace with valid value
                mailMessage.Subject = "Otp Verification";
                mailMessage.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                using (var smtp = new SmtpClient())
                {

                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mailMessage);
                }
            });
            thread.IsBackground = true;
            thread.Start();


        }
        [HttpPost]
        public IHttpActionResult VerifyOtp(GuardianViewModel model)
        {
            var guardian = _context.Guardians.FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber && x.MobileOTP == model.MobileOtp);
            if (guardian != null)
            {
                guardian.IsOtpVerified = true;
                _context.SaveChanges();
                return Json("success");
            }
            return Json("error");
        }
        public Notification SaveNotification(Guid guardianId, string message, string grade, string sectionId, Guid studentId, string status)
        {
            var NewNotification = new Notification();
            NewNotification.StudentId = studentId;
            NewNotification.GuardianId = guardianId;
            NewNotification.Message = message;
            NewNotification.InStatus = status;
            NewNotification.NotificationDate = DateTime.Now;
            NewNotification.SectionId = Convert.ToInt32(sectionId);
            NewNotification.GradeId = Convert.ToInt32(grade);
            NewNotification.NotificationFrom = "Mobile App";
            NewNotification.GateId = 1;
            NewNotification.IsNotified = false;
            _context.Notifications.Add(NewNotification);
            _context.SaveChanges();

            return NewNotification;
        }
        public string SendOtp(string phoneNumber, bool isForgotPassword = false)
        {

            var guardian = _context.Guardians.FirstOrDefault(x => x.PhoneNumber == phoneNumber);
            string mobileOtp = string.Empty;
            if (guardian != null)
            {
                try
                {


                    string baseurl = string.Empty;
                    if (isForgotPassword)
                    {
                        mobileOtp = guardian.MobileOTP;
                    }
                    else
                    {
                        mobileOtp = GenerateOtp();
                    }
                    string message = "Your password for login is " + mobileOtp + ". Your mobile number is verified.";
                    WebClient client = new WebClient();
                    if (isDevMode)
                    {
                        phoneNumber = "91" + phoneNumber;
                        baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=kapsystem&password=kap@user!123&sender=KAPNFO&SMSText=" + message + "&GSM=" + phoneNumber;
                    }
                    else
                    {
                        phoneNumber = phoneNumber.Substring(3);
                        baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=internationalsms&password=wbBGfCLE&sender=KAPINT&SMSText=" + message + "&type=longsms&GSM=" + phoneNumber;
                        var body = "<p>Hi ,</p><p><p>Your password is " + mobileOtp + "</p>";
                        body = body + "<p>Best Regards</p><p>Misk School Team</p>";
                        SendOtpToEmail("miskschooltesting@gmail.com", body);
                    }
                    Stream data = client.OpenRead(baseurl);
                    StreamReader reader = new StreamReader(data);
                    string response = reader.ReadToEnd();
                    data.Close();
                    reader.Close();
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(response);
                    XmlNode statusNode = xmlDoc.SelectSingleNode("/results/result/status");
                    string statusCode = statusNode.InnerText;
                    guardian.MobileOTP = mobileOtp;
                    guardian.IsOtpVerified = false;
                    _context.SaveChanges();
                    return statusCode;
                }
                catch (Exception ex)
                {
                    return "ExceptionOccuredInSmsGateWay";
                }

            }
            return "";
        }
        protected string GenerateOtp()
        {

            string numbers = "123456789";
            int length = 6;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, numbers.Length);
                    character = numbers.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }
        public static double Calculate(double lat1, double long1, double lat2, double long2)
        {
            double R = 6371;
            double dLat = ToRad((lat2 - lat1));
            double dLon = ToRad((long2 - long1));
            lat1 = ToRad(lat1);
            lat2 = ToRad(lat2);
            double a = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(lat1) * Math.Cos(lat2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c * 1000;
            return d;
        }

        public static double ToRad(double value)
        {
            return value * Math.PI / 180;
        }
    }
}
