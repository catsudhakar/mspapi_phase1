﻿using MSPAPI.Models;
using MSPAPI.ViewModel;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web.Http;
using System.Xml;

namespace MSPAPI.Controllers
{
    public class AccountController : ApiController
    {
        bool isDevMode = Convert.ToBoolean(ConfigurationManager.AppSettings["DevMode"]);
        MspTestPortalEntities _context = new MspTestPortalEntities();

        public AccountController()
        {
            if (isDevMode)
            {
                _context.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            }
        }
        [HttpPost]
        public IHttpActionResult Login(GuardianViewModel model)
        {
            try
            {
                var objGuardian = _context.Guardians
                     .FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber && x.MobileOTP == model.MobileOtp);
                if (objGuardian != null)
                {
                    return Json("Success");
                }
                else
                {
                    return Json("Error");
                }

            }
            catch (Exception ex)
            {

                return Json("");
            }
        }
        [HttpPost]
        public IHttpActionResult ForgotPassword(GuardianViewModel model)
        {
            try
            {
                var objGuardian = _context.Guardians
                     .FirstOrDefault(x => x.PhoneNumber == model.PhoneNumber);
                if (objGuardian != null)
                {
                    string status = SendOtp(model.PhoneNumber, true);
                    if (status != "0")
                    {
                        return Json("OtpSendingFailed");
                    }
                    return Json(objGuardian.MobileOTP);
                }
                else
                {
                    return Json("PhNoNotExists");
                }

            }
            catch (Exception ex)
            {

                return Json("");
            }
        }
        [HttpPost]
        public IHttpActionResult ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                var objGuardian = _context.Guardians
                     .FirstOrDefault(x => x.Id == model.GuardianId && x.MobileOTP == model.PreviousPassword);
                if (objGuardian != null)
                {
                    objGuardian.MobileOTP = model.NewPassword;
                    _context.SaveChanges();
                    return Json("Success");
                }
                else
                {
                    return Json("PrevPasswordNotMatched");
                }

            }
            catch (Exception ex)
            {

                return Json("");
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateMobileNumber1(MobileViewModel model)
        {
            var guardian = _context.Guardians.FirstOrDefault(x => x.Id == model.GuardianId && x.PhoneNumber==model.NewPhoneNumber);
            if (guardian != null)
            {
                guardian.PhoneNumber = model.NewPhoneNumber;
                _context.SaveChanges();
                return Json("Success");
            }
            else
            {
                return Json("PhNoNotMatched");
            }

        }

        [HttpPost]
        public IHttpActionResult UpdateMobileNumber(MobileViewModel model)
        {
            var guardian = _context.Guardians.FirstOrDefault(x => x.Id == model.GuardianId && x.PhoneNumber == model.OldPhoneNumber);
            if (guardian != null)
            {
                guardian.PhoneNumber = model.NewPhoneNumber;
                _context.SaveChanges();
                return Json("Success");
            }
            else
            {
                return Json("PhNoNotMatched");
            }

        }
        public string SendOtp(string phoneNumber, bool isForgotPassword = false)
        {

            var guardian = _context.Guardians.FirstOrDefault(x => x.PhoneNumber == phoneNumber);
            string mobileOtp = string.Empty;
            if (guardian != null)
            {
                string baseurl = string.Empty;
                if (isForgotPassword)
                {
                    mobileOtp = guardian.MobileOTP;
                }
                else
                {
                    mobileOtp = GenerateOtp();
                }
                string message = "Your password for login is " + mobileOtp + ". Your mobile number is verified.";
                WebClient client = new WebClient();
                if (isDevMode)
                {
                    phoneNumber = "91" + phoneNumber;
                    baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=kapsystem&password=kap@user!123&sender=KAPNFO&SMSText=" + message + "&GSM=" + phoneNumber;
                }
                else
                {
                    phoneNumber = phoneNumber.Substring(3);
                    baseurl = "http://193.105.74.159/api/v3/sendsms/plain?user=internationalsms&password=wbBGfCLE&sender=KAPINT&SMSText=" + message + "&type=longsms&GSM=" + phoneNumber;
                    var body = "<p>Hi ,</p><p><p>Your password is " + mobileOtp + "</p>";
                    body = body + "<p>Best Regards</p><p>Misk School Team</p>";
                    SendOtpToEmail("miskschooltesting@gmail.com", body);
                }
                Stream data = client.OpenRead(baseurl);
                StreamReader reader = new StreamReader(data);
                string response = reader.ReadToEnd();
                data.Close();
                reader.Close();
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(response);
                XmlNode statusNode = xmlDoc.SelectSingleNode("/results/result/status");
                string statusCode = statusNode.InnerText;
                guardian.MobileOTP = mobileOtp;
                guardian.IsOtpVerified = false;
                _context.SaveChanges();
                return statusCode;

            }
            return "";
        }
        public void SendOtpToEmail(string email, string body)
        {
            Thread thread = new Thread(delegate ()
            {
                var mailMessage = new MailMessage();
                mailMessage.To.Add(new MailAddress(email));
                //replace with valid value
                mailMessage.Subject = "Otp Verification";
                mailMessage.From = new MailAddress("noreply.lrisf@gmail.com", "Misk Schools");
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                using (var smtp = new SmtpClient())
                {

                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential("noreply.lrisf@gmail.com", "Friday$1");
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    smtp.Send(mailMessage);
                }
            });
            thread.IsBackground = true;
            thread.Start();


        }
        protected string GenerateOtp()
        {

            string numbers = "123456789";
            int length = 6;
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, numbers.Length);
                    character = numbers.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

    }
}
