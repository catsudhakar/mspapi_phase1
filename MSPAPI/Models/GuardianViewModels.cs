﻿
using MSPAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSPAPI.Models
{

    public enum StudentStatus
    {
        Waiting = 1,
        InQueue = 2,
        Dismiss = 3,
        PickedUp = 4,

    }

    public class LatLangViewModel
    {
        public double CurrentLattitude { get; set; }
        public double CurrentLongitude { get; set; }
        public Guid GuardianId { get; set; }
    }
    public class DriverDetails
    {
        public string DeviceId { get; set; }
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
        public string Id { get; set; }
    }

    public class DateViewModel
    {
        public string Date { get; set; }
        public List<StudentViewModel> StudentList { get; set; }
    }
    public class StudentsByDateViewModel
    {
        public string Date { get; set; }
        public List<StudentViewModel> StudentList { get; set; }
    }
    public class ReportProblemViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public Guid ReporterId { get; set; }
    }
    public class ChangePasswordViewModel
    {
        public Guid GuardianId { get; set; }
        public string PreviousPassword { get; set; }
        public string NewPassword { get; set; }
    }


}